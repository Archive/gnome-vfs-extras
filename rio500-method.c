/* rio500-method.h - VFS modules for Rio500
 *
 *  Copyright (C) 2001,2002 Bastien Nocera
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Author: Bastien Nocera <hadess@hadess.net>
 * Inspired by Alex Larsson's neato SMB vfs method
 */

#include "config.h"
#include "gnome-vfs-extras-i18n.h"

/* libgen first for basename */
#include <libgen.h>
#include <string.h>
#include <glib.h>

#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime.h>

#include <libgnomevfs/gnome-vfs-method.h>
#include <libgnomevfs/gnome-vfs-module.h>
#include <libgnomevfs/gnome-vfs-module-shared.h>
#include <libgnomevfs/gnome-vfs-module-callback-module-api.h>
#include <libgnomevfs/gnome-vfs-standard-callbacks.h>

/* $(includedir)/rio500 should be in the CFLAGS */
#include <librio500_api.h>

typedef struct _RioContent RioContent;

struct _RioContent {
	GList *content;

	/* these aren't used for the virt content */
	gulong mem_total, mem_free;
	gboolean dirty;
};

typedef struct _RioTreeContent RioTreeContent;

struct _RioTreeContent {
	Rio500 *rio_hw;
	gboolean have_flash;
	RioContent *internal, *external, *virtual;
};

/* Global vars: */
static GMutex *rio_lock = NULL;
static RioTreeContent *rio = NULL;

/*#define DEBUG_RIO_ENABLE
#define DEBUG_RIO_LOCKS */

#ifdef DEBUG_RIO_ENABLE
#define DEBUG_RIO(x) g_print x
#else
#define DEBUG_RIO(x) 
#endif

#ifdef DEBUG_RIO_LOCKS
#define LOCK_RIO() 	{g_mutex_lock (rio_lock); g_print ("LOCK %s\n", G_GNUC_PRETTY_FUNCTION);}
#define UNLOCK_RIO() 	{g_print ("UNLOCK %s\n", G_GNUC_PRETTY_FUNCTION); g_mutex_unlock (rio_lock);}
#else
#define LOCK_RIO() 	g_mutex_lock (rio_lock)
#define UNLOCK_RIO() 	g_mutex_unlock (rio_lock)
#endif

static gboolean update_tree_content ();
static void destroy_rio ();
static char *gnome_vfs_uri_get_basename (const GnomeVFSURI *uri);

//FIXME see smb-method.c
static char *
gnome_vfs_uri_get_basename (const GnomeVFSURI *uri)
{
	DEBUG_RIO(("gnome_vfs_uri_get_basename: uri: %s, basename: %s\n",
				uri->text, basename (uri->text)));
	return basename (uri->text);
}

static GList *
rio_get_content_from_card (int card)
{
	if (card == 0)
		return rio->internal->content;

	if ((card == 1) && (rio->have_flash == TRUE))
		return rio->external->content;

	return NULL;
}

static RioContent *
get_content_from_card (int card)
{
	return (card == 0 ? rio->internal: rio->external);
}

static GnomeVFSResult
lookup_uri(GnomeVFSURI *uri, int *card, int *folder, int *song)
{
	char *cardname;
	const char *path;
	GList *content, *list;
	char **str;
	RioFolderEntry *folder_entry = NULL;
	RioSongEntry *song_entry;

	*card = *folder = *song = -1;

	DEBUG_RIO(("lookup_uri: %s\n", gnome_vfs_uri_to_string(uri, 0)));

	if (update_tree_content() == FALSE)
		return GNOME_VFS_ERROR_IO;

	LOCK_RIO();
	if (rio == NULL)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_IO;
	}

	cardname = (char *) gnome_vfs_uri_get_host_name (uri);
	if (cardname != NULL)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_INVALID_URI;
	}

	/* Is this root? */
	if (gnome_vfs_uri_has_parent (uri) == 0)
	{
		UNLOCK_RIO();
		return GNOME_VFS_OK;
	}

	{
		const char *tmp;

		tmp = gnome_vfs_uri_get_path(uri);
		path = gnome_vfs_unescape_string(tmp, "/");
		if (path == NULL)
			path = tmp;

		DEBUG_RIO(("lookup_uri: path %s\n", path));
	}

	str = g_strsplit(path, "/", 0);
	/* this is pretty safe i think */
	cardname = g_strdup(str[1]);

	/* now we are in a flash card... maybe */
	DEBUG_RIO(("lookup_uri: cardname %s\n", cardname));

	/* Translators: Use only ASCII characters for this,
	 * or don't translate */
	if (g_strcasecmp(cardname, _("Internal")) == 0)
	{
		/* internal card */
		DEBUG_RIO(("lookup_uri: internal card detected\n"));
		*card = 0;
		content = rio->internal->content;
	} else {
		/* Translators: Use only ASCII characters for this,
		 * or don't translate */
		if (!g_strcasecmp(cardname, _("External")) &&
				rio->have_flash == TRUE)
		{
			DEBUG_RIO(("lookup_uri: external card detected\n"));
			/* external card */
			*card = 1;
			content = rio->external->content;
		} else {
			DEBUG_RIO(("lookup_uri: nonexistant card detected\n"));
			/* none existant directory */
			g_free(cardname);
			g_strfreev(str);
			UNLOCK_RIO();
			return GNOME_VFS_ERROR_NOT_FOUND;
		}
	}
	g_free(cardname);

	if (str[2] == NULL || str[2][0] == '\0')
	{
		g_strfreev(str);
		UNLOCK_RIO();
		return GNOME_VFS_OK;
	}

	/* look for the folder name */
	DEBUG_RIO(("lookup_uri: looking up folder '%s'\n", str[2]));
	for (list = content; list != NULL; list = list->next)
	{
		folder_entry = (RioFolderEntry *) list->data;

		if (folder_entry
				&& (strcmp(str[2], folder_entry->name) == 0))
		{
			DEBUG_RIO(("lookup_uri: found folder %d\n", folder_entry->folder_num));
			/* match the folder */
			*folder = folder_entry->folder_num;
			break;
		}
	}

	/* if the folder didn't match anything on the rio, go "404" */
	if (*folder == -1)
	{
		g_strfreev(str);
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_NOT_FOUND;
	}

	/* is it a directory ? */
	if (str[3] == NULL || str[3][0] == '\0')
	{
		g_strfreev(str);
		UNLOCK_RIO();
		return GNOME_VFS_OK;
	}

	/* look for the song */
	for (list = folder_entry->songs; list != NULL; list = list->next)
	{
		song_entry = (RioSongEntry *) list->data;
		if (song_entry
				&& (strcmp(str[3], song_entry->name) == 0))
		{
			/* match the song */
			*song = song_entry->song_num;
			break;
		}
	}
	/* if the song didn't match anything on the rio, go "404" */
	if (*song == -1)
	{
		g_strfreev(str);
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_NOT_FOUND;
	}

	/* oh my ! too much information there */
	if (str[4] != NULL)
	{
		g_strfreev(str);
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_INVALID_URI;
	}

	g_strfreev(str);
	UNLOCK_RIO();
	return GNOME_VFS_OK;
}

static void
add_folder (RioContent *content, int card, const char *name)
{
	RioFolderEntry *folder_entry = NULL;

	DEBUG_RIO(("add_folder: %s\n", name));

	folder_entry = g_new(RioFolderEntry, 1);
	folder_entry->name = g_strdup (name);
	folder_entry->folder_num = card;
	folder_entry->songs = NULL;
	content->content = g_list_append (content->content,
			(gpointer) folder_entry);
}

static RioContent
*update_virtual_content (RioContent *content, gboolean have_flash)
{
	DEBUG_RIO(("update_virtual_content\n"));
	/* FIXME we don't support having a different number of cards */
	if (content != NULL)
		return content;

	content = g_new(RioContent, 1);
	content->content = NULL;
	add_folder (content, 0, _("Internal"));
	if (have_flash == TRUE)
	{
		add_folder (content, 1, _("External"));
	}

	return content;
}

static RioContent
*update_content (RioContent *content, int card)
{
	DEBUG_RIO(("update_content: updating card %d\n", card));
	if (content != NULL)
	{
		if (content->content != NULL)
		{
			rio_destroy_content (content->content);
			content->content = NULL;
		}
		content->dirty = FALSE;
		content->mem_total = content->mem_free = 0;
	} else {
		content = g_new (RioContent, 1);
	}

	rio_set_card (rio->rio_hw, card);
	content->mem_free = rio_memory_left (rio->rio_hw);
	content->mem_total = rio_get_mem_total (rio->rio_hw);
	content->content = rio_get_content (rio->rio_hw);
	content->dirty = FALSE;

	return content;
}

static gboolean
update_tree_content()
{
	DEBUG_RIO(("update_tree_content start\n"));
	LOCK_RIO();

	rio->rio_hw = rio_new();
	if (rio_check (rio->rio_hw) == FALSE)
	{
		DEBUG_RIO(("update_tree_content: check of rio is FALSE, destroying\n"));
		UNLOCK_RIO();
		return FALSE;
	}
#if 0
	{
		int major, minor;
		rio_get_revision (rio->rio_hw, &major, &minor);
		DEBUG_RIO(("update_tree_content: version %d.%d\n",
					major, minor));
	}
#endif
	/* Update card number */
	if (rio_get_card_number (rio->rio_hw) >= 2)
		rio->have_flash = TRUE;
	DEBUG_RIO(("update_tree_content: got internal card info\n"));

	/* Update virtual folders */
	if (rio->virtual == NULL)
	{
		rio->virtual = update_virtual_content (rio->virtual,
				rio->have_flash);
	}

	/* Internal and external rams */
	if (rio->internal == NULL || rio->internal->dirty == TRUE)
	{
		rio->internal = update_content (rio->internal, 0);
	}
	if (rio->have_flash == TRUE)
	{
		if (rio->external == NULL || rio->external->dirty == TRUE)
		{
			rio->external = update_content (rio->external, 1);
		}
	}

	rio_delete(rio->rio_hw);
	UNLOCK_RIO();

	return TRUE;
}

static void
destroy_rio()
{
	g_return_if_fail(rio != NULL);

	DEBUG_RIO(("destroy_rio: start\n"));
	if (rio->internal->content != NULL)
		rio_destroy_content(rio->internal->content);
	DEBUG_RIO(("destroy_rio: destroyed int_content\n"));
	if (rio->have_flash == TRUE
			&& rio->external->content != NULL)
		rio_destroy_content(rio->external->content);
	DEBUG_RIO(("destroy_rio: destroyed ext_content\n"));
	if (rio->virtual->content != NULL)
		rio_destroy_content(rio->virtual->content);
	DEBUG_RIO(("destroy_rio: destroyed virt_content\n"));

	g_free (rio->internal);
	g_free (rio->external);
	g_free (rio->virtual);

	g_free(rio);
	rio = NULL;

	DEBUG_RIO(("destroy_rio: end\n"));
}

static GnomeVFSResult
rio_to_vfs_error(int error)
{
	switch (error)
	{
	case RIO_FILEERR:
		return GNOME_VFS_ERROR_IO;
	case RIO_FORMAT:
		return GNOME_VFS_ERROR_GENERIC;
	case RIO_ENDCOMM:
		return GNOME_VFS_ERROR_INTERNAL;
	case RIO_INITCOMM:
		return GNOME_VFS_ERROR_INTERNAL;
	case RIO_NODIR:
		return GNOME_VFS_ERROR_NOT_FOUND;
	case RIO_NOMEM:
		return GNOME_VFS_ERROR_NO_SPACE;
	case PC_MEMERR:
		return GNOME_VFS_ERROR_NO_MEMORY;
	case RIO_SUCCESS:
		return GNOME_VFS_OK;
	}

	g_warning("Rio generated error %d not found\n", error);
	return GNOME_VFS_ERROR_GENERIC;
}

typedef struct {
	/* is it a read or a write to the rio500 */
	gboolean is_read;
	/* handle for the temp file */
	GnomeVFSHandle *handle;
	/* For writing */
	char *filename;
	int folder;
	int space_left;
	int card;
} FileHandle;

static GnomeVFSResult
do_open (GnomeVFSMethod *method,
	 GnomeVFSMethodHandle **method_handle,
	 GnomeVFSURI *uri,
	 GnomeVFSOpenMode mode,
	 GnomeVFSContext *context)
{
	FileHandle *handle = NULL;
	GnomeVFSResult res;
	int card, folder, song;
	gboolean is_read = FALSE;

	DEBUG_RIO(("do_open() %s mode %d\n",
				gnome_vfs_uri_to_string (uri, 0), mode));

	if (g_file_test ("/tmp/rio500_tmp_file.mp3", G_FILE_TEST_EXISTS)
			== TRUE)
		return GNOME_VFS_ERROR_TOO_MANY_OPEN_FILES;

	if (mode & GNOME_VFS_OPEN_READ)
		is_read = TRUE;
	else if (mode & GNOME_VFS_OPEN_WRITE)
		is_read = FALSE;
	else
		return GNOME_VFS_ERROR_INVALID_OPEN_MODE;

	res = lookup_uri(uri, &card, &folder, &song);
	DEBUG_RIO(("do_open(): lookup %d %d %d\n",
				card, folder, song));

	LOCK_RIO();

	if ((res != GNOME_VFS_OK) && (is_read == TRUE))
	{
		UNLOCK_RIO();
		DEBUG_RIO(("do_open(): file not found on read open, result\n"));
		return res;
	}

	if ((folder != -1) && (song == -1) && (is_read == TRUE))
	{
		UNLOCK_RIO();
		DEBUG_RIO(("do_open(): file is a directory on a read open\n"));
		return GNOME_VFS_ERROR_IS_DIRECTORY;
	}

	if ((card != -1) && (folder == -1) && (is_read == FALSE))
	{
		/* trying to create a file under rio500://internal/
		 * like: rio500://internal/foo.mp3 <- that won't work */
		UNLOCK_RIO();
		DEBUG_RIO(("do_open(): tried to create a file without creating"
					" a folder first\n"));
		return GNOME_VFS_ERROR_NOT_PERMITTED;
	}

	if ((song == -1) && (is_read == TRUE))
	{
		UNLOCK_RIO();
		DEBUG_RIO(("do_open(): file not found on a read open\n"));
		return GNOME_VFS_ERROR_NOT_FOUND;
	}

	/* Give a try at opening the device first */
	rio->rio_hw = rio_new();
	if (rio_check(rio->rio_hw) == FALSE)
	{
		DEBUG_RIO(("do_open: check of rio is FALSE\n"));
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_IO;
	}

	if (is_read == TRUE)
	{
		GnomeVFSHandle *local_handle;
		int res;

		DEBUG_RIO(("do_open: opening read-only\n"));

		rio_set_card(rio->rio_hw, card);
		res = rio_get_song(rio->rio_hw, "/tmp/rio500_tmp_file.mp3",
				folder, song);
		rio_delete (rio->rio_hw);

		if (res != RIO_SUCCESS)
		{
			unlink ("/tmp/rio500_tmp_file.mp3");
			UNLOCK_RIO();
			DEBUG_RIO(("do_open(): error getting song from"
						"the rio\n"));
			return GNOME_VFS_ERROR_IO;
		}

		DEBUG_RIO(("do_open: copied to /tmp/rio500_tmp_file.mp3\n"));

		if (gnome_vfs_open (&local_handle, "/tmp/rio500_tmp_file.mp3",
				GNOME_VFS_OPEN_READ) != GNOME_VFS_OK)
		{
			unlink ("/tmp/rio500_tmp_file.mp3");
			UNLOCK_RIO();
			DEBUG_RIO(("do_open(): error opening the tmp file\n"));
			return GNOME_VFS_ERROR_IO;
		}

		DEBUG_RIO(("do_open: opened local file\n"));

		/* Create the handle */
		handle = g_new(FileHandle, 1);
		/* global */
		handle->is_read = is_read;
		handle->handle = local_handle;
		handle->filename = NULL;
	} else {
		GnomeVFSHandle *local_handle;
		char *basename, *local;

		/* We don't need the rio_hw immediatly in write mode */
		rio_delete (rio->rio_hw);

		basename = gnome_vfs_uri_get_basename (uri);
		local = g_strdup_printf ("file://%s/%s", g_get_tmp_dir(), basename);
		if (gnome_vfs_create (&local_handle, local,
		    GNOME_VFS_OPEN_WRITE, FALSE, 0600) != GNOME_VFS_OK)
		{
			UNLOCK_RIO();
			DEBUG_RIO(("do_open(): error opening the temp file for writing %s\n", local));
			g_free (local);
			return GNOME_VFS_ERROR_IO;
		}

		/* Create the handle */
		handle = g_new(FileHandle, 1);
		handle->is_read = is_read;
		handle->handle = local_handle;
		handle->folder = folder;
		handle->filename = gnome_vfs_get_local_path_from_uri (local);
		handle->space_left = (card == 0 ? rio->internal->mem_free :
				rio->external->mem_free);
		handle->card = card;

		g_free (local);
	}

	*method_handle = (GnomeVFSMethodHandle *)handle;
	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_close (GnomeVFSMethod *method,
	  GnomeVFSMethodHandle *method_handle,
	  GnomeVFSContext *context)

{
	FileHandle *handle = (FileHandle *)method_handle;
	GnomeVFSResult res = GNOME_VFS_OK;

	DEBUG_RIO(("do_close(): is_read: %d\n", handle->is_read));

	if (handle->is_read == TRUE)
	{
		int tmp;
		
		DEBUG_RIO(("do_close(): unlinking temp file\n"));
		tmp = unlink ("/tmp/rio500_tmp_file.mp3");
		if (tmp != 0)
			res = GNOME_VFS_ERROR_IO;
		else
			res = GNOME_VFS_OK;
	} else if (handle->space_left > 0) {
		int tmp;

		DEBUG_RIO(("do_close(): uploading song %s to folder %d\n",
					handle->filename, handle->folder));

		rio->rio_hw = rio_new();
		if (rio_check(rio->rio_hw) == FALSE)
		{
			unlink (handle->filename);
			DEBUG_RIO(("do_close: check of rio is FALSE\n"));
			UNLOCK_RIO();
			return GNOME_VFS_ERROR_IO;
		}

		rio_set_card(rio->rio_hw, handle->card);
		tmp = rio_add_song (rio->rio_hw, handle->folder,
				handle->filename);
		DEBUG_RIO(("do_close(): rio_add_song(..., %d, %s)\n",
					handle->folder, handle->filename));
		rio_delete(rio->rio_hw);

		(get_content_from_card (handle->card)->dirty) = TRUE;

		unlink (handle->filename);
		res = rio_to_vfs_error(tmp);
		DEBUG_RIO(("do_close(): uploading result: %s (rio: %d)\n",
					gnome_vfs_result_to_string(res), tmp));
	} else {
		DEBUG_RIO(("do_close(): not uploading, no memory\n"));
		unlink (handle->filename);
		res = GNOME_VFS_ERROR_NO_SPACE;
	}

	/* delete the handle's content */
	gnome_vfs_close (handle->handle);
	g_free (handle->filename);
	g_free (handle);

	UNLOCK_RIO();

	return res;
}

static GnomeVFSResult
do_read (GnomeVFSMethod *method,
	 GnomeVFSMethodHandle *method_handle,
	 gpointer buffer,
	 GnomeVFSFileSize num_bytes,
	 GnomeVFSFileSize *bytes_read,
	 GnomeVFSContext *context)
{
	FileHandle *handle = (FileHandle *)method_handle;

	DEBUG_RIO(("do_read() %Lu bytes\n", num_bytes));

	return gnome_vfs_read (handle->handle, buffer, num_bytes, bytes_read);
}

static GnomeVFSResult
do_write (GnomeVFSMethod *method,
	  GnomeVFSMethodHandle *method_handle,
	  gconstpointer buffer,
	  GnomeVFSFileSize num_bytes,
	  GnomeVFSFileSize *bytes_written,
	  GnomeVFSContext *context)


{
	GnomeVFSResult result;
	FileHandle *handle = (FileHandle *)method_handle;

	DEBUG_RIO (("do_write() %p\n", method_handle));

	/* Check if we have enough space left for that much on the rio */
	DEBUG_RIO (("do_write() space left: %d / bytes: %d / diff: %d\n",
				handle->space_left, (int) num_bytes,
				(int) (handle->space_left - num_bytes)));

	if ((handle->space_left - ((int) num_bytes)) <= 0)
	{
		DEBUG_RIO (("do_write(): no more space left\n"));
		/* Set it so that it's negative */
		handle->space_left -= (int) num_bytes;
		return GNOME_VFS_ERROR_NO_SPACE;
	}

	result = gnome_vfs_write (handle->handle, buffer, num_bytes,
			bytes_written);

	handle->space_left -= (int) (*bytes_written);
	DEBUG_RIO (("do_write(): %d\n", (int) (handle->space_left)));
	return result;
}

static GnomeVFSResult
do_create (GnomeVFSMethod *method,
	   GnomeVFSMethodHandle **method_handle,
	   GnomeVFSURI *uri,
	   GnomeVFSOpenMode mode,
	   gboolean exclusive,
	   guint perm,
	   GnomeVFSContext *context)

{
	DEBUG_RIO (("do_create() %s mode %d\n",
				gnome_vfs_uri_to_string (uri, 0), mode));


	return do_open (method, method_handle, uri, GNOME_VFS_OPEN_WRITE,
			context);
}

static GnomeVFSResult
do_get_file_info (GnomeVFSMethod *method,
		  GnomeVFSURI *uri,
		  GnomeVFSFileInfo *file_info,
		  GnomeVFSFileInfoOptions options,
		  GnomeVFSContext *context)

{
	GnomeVFSResult res;
	int card, folder, song;

	DEBUG_RIO (("do_get_file_info() %s\n",
				gnome_vfs_uri_to_string (uri, 0)));

	res = lookup_uri(uri, &card, &folder, &song);
	DEBUG_RIO(("do_get_file_info(): lookup %d %d %d\n",
				card, folder, song));

	LOCK_RIO();
	if (res != GNOME_VFS_OK)
	{
		UNLOCK_RIO();
		return res;
	}

	file_info->valid_fields =
		GNOME_VFS_FILE_INFO_FIELDS_TYPE |
		GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;

	/* Is this root? */
	if (gnome_vfs_uri_has_parent (uri) == 0) {
		file_info->name = g_strdup ("/");
		file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_TYPE |
			GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;
		file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
		file_info->mime_type = g_strdup ("x-directory/normal");
		UNLOCK_RIO();
		return GNOME_VFS_OK;
	}

	/* is it a folder */
	if (song == -1)
	{
		/* is it not inside a card ? */
		if (folder == -1)
		{
			file_info->name = g_strdup ((card == 0) ?
					_("Internal") :
					_("External"));
			file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
			file_info->mime_type = g_strdup ("x-directory/normal");
		} else {
			file_info->name = g_strdup
				(gnome_vfs_uri_extract_short_name (uri));
			file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
			file_info->mime_type = g_strdup ("x-directory/normal");
		}
	} else {
		file_info->name = g_strdup
			(gnome_vfs_uri_extract_short_name (uri));
		file_info->type = GNOME_VFS_FILE_TYPE_REGULAR;
		file_info->mime_type = g_strdup ("audio/x-mp3");
	}

	UNLOCK_RIO();

	return GNOME_VFS_OK;
}

static gboolean
do_is_local (GnomeVFSMethod *method,
	     const GnomeVFSURI *uri)
{
	DEBUG_RIO (("do_is_local(): %s\n", gnome_vfs_uri_to_string (uri,
					GNOME_VFS_URI_HIDE_NONE)));

	return FALSE;
}

typedef struct {
	GList *list;
	char *dirname;
	guint pos;
	gboolean list_type_is_folder;
} DirectoryHandle;

static GnomeVFSResult
do_open_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle **method_handle,
		   GnomeVFSURI *uri,
		   GnomeVFSFileInfoOptions options,
		   GnomeVFSContext *context)

{
	GnomeVFSResult res;
	DirectoryHandle *directory_handle;
	GList *list = NULL;
	int card, folder, song;
	const char *path; //FIXME
	gboolean list_type_is_folder = TRUE;
	
	DEBUG_RIO(("do_open_directory() %s\n",
		gnome_vfs_uri_to_string (uri, 0)));

	res = lookup_uri(uri, &card, &folder, &song);
	DEBUG_RIO(("do_open_directory(): lookup %d %d %d (result: %s)\n",
				card, folder, song,
				gnome_vfs_result_to_string (res)));
	if (res != GNOME_VFS_OK)
	{
		return res;
	}

	LOCK_RIO();
	if (song != -1)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_NOT_A_DIRECTORY;
	}

	if (folder == -1)
	{
		/* can be rio500: or rio500://internal/ */
		if (card == -1)
		{
			list = rio->virtual->content;
			path = "/";
		} else {
			if (card == 1 && rio->have_flash == FALSE)
			{
				UNLOCK_RIO();
				return GNOME_VFS_ERROR_NOT_FOUND;
			}
			if (card == 0)
			{
				list = rio->internal->content;
				path = _("Internal");
			} else {
				list = rio->external->content;
				path = _("External");
			}
		}
	} else {
		GList *content = NULL;
		RioFolderEntry *entry = NULL;

		list_type_is_folder = FALSE;

		content = rio_get_content_from_card (card);

		entry = (RioFolderEntry *) g_list_nth_data(content, folder);
		path = entry->name;
		list = entry->songs;
	}

	/* Construct the handle */
	directory_handle = g_new0 (DirectoryHandle, 1);
	directory_handle->dirname = g_strdup (path);
	directory_handle->pos = 0;
	directory_handle->list = list;
	directory_handle->list_type_is_folder = list_type_is_folder;
	*method_handle = (GnomeVFSMethodHandle *) directory_handle;

	UNLOCK_RIO();

	DEBUG_RIO(("do_open_directory end\n"));
	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_close_directory (GnomeVFSMethod *method,
		    GnomeVFSMethodHandle *method_handle,
		    GnomeVFSContext *context)
{
	DirectoryHandle *directory_handle = (DirectoryHandle *) method_handle;

	DEBUG_RIO(("do_close_directory: %p\n", directory_handle));

	if (directory_handle == NULL)
		return GNOME_VFS_OK;

	//FIXME
	g_free (directory_handle->dirname);
	g_free (directory_handle);
	DEBUG_RIO(("do_close_directory: end\n"));

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_read_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle *method_handle,
		   GnomeVFSFileInfo *file_info,
		   GnomeVFSContext *context)
{
	DirectoryHandle *dh = (DirectoryHandle *) method_handle;
	GList *list;
	RioFolderEntry *folder_entry;
	RioSongEntry *song_entry;

	DEBUG_RIO (("do_read_directory()\n"));

	LOCK_RIO();
	if (dh->pos > (g_list_length(dh->list)+2) )
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_EOF;
	}

	/* . or .. directories */
	if (dh->pos == 0 || dh->pos == 1)
	{
		file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_NONE |
			GNOME_VFS_FILE_INFO_FIELDS_TYPE |
			GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;

		file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
		file_info->name = g_strdup ((dh->pos == 0) ? "." : "..");
		file_info->mime_type = g_strdup ("x-directory/normal");
		DEBUG_RIO (("do_read_directory(): read folder %s\n", file_info->name));
		dh->pos++;
		UNLOCK_RIO();
		return GNOME_VFS_OK;
	}

	list = g_list_nth(dh->list, dh->pos - 2);
	if (list == NULL)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_EOF;
	}

	GNOME_VFS_FILE_INFO_SET_LOCAL (file_info, FALSE);

	/* Tell gnome-vfs which fields will be valid */
	file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_NONE |
		GNOME_VFS_FILE_INFO_FIELDS_TYPE |
		GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;

	/* Fill the fields */
	if (dh->list_type_is_folder == TRUE)
	{
		folder_entry = (RioFolderEntry *)(list->data);
		file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
		file_info->name = g_strdup (folder_entry->name);
		file_info->mime_type = g_strdup ("x-directory/normal");
		DEBUG_RIO (("do_read_directory(): read folder %s\n", file_info->name));
	} else {
		file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_SIZE;
		song_entry = (RioSongEntry *)(list->data);
		file_info->type = GNOME_VFS_FILE_TYPE_REGULAR;
		file_info->name = g_strdup(song_entry->name);
		file_info->size = song_entry->size;
		file_info->mime_type = g_strdup("audio/x-mp3");
		DEBUG_RIO (("do_read_directory(): read file %s\n", file_info->name));
	}

	dh->pos++;

	DEBUG_RIO (("do_read_directory(): position %d\n", dh->pos));
	UNLOCK_RIO();
	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_unlink (GnomeVFSMethod *method,
	   GnomeVFSURI *uri,
	   GnomeVFSContext *context)
{
  	GnomeVFSResult res;
	int card, folder, song;

	DEBUG_RIO (("do_unlink() %s\n",
				gnome_vfs_uri_to_string (uri, 0)));

	res = lookup_uri(uri, &card, &folder, &song);
	DEBUG_RIO(("do_unlink(): lookup %d %d %d\n",
				card, folder, song));

	LOCK_RIO();
	if (res != GNOME_VFS_OK)
	{
		UNLOCK_RIO();
		return res;
	}

	if (folder == -1)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_NOT_PERMITTED;
	}

	if (song == -1)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_NOT_A_DIRECTORY;
	}

	rio->rio_hw = rio_new();
	if (rio_check(rio->rio_hw) == FALSE)
	{
		DEBUG_RIO(("do_unlink: check of rio is FALSE, destroying\n"));
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_IO;
	}
	rio_set_card(rio->rio_hw, card);
	res = rio_to_vfs_error(rio_del_song(rio->rio_hw,
				folder, song));
	rio_delete(rio->rio_hw);

	if (res == GNOME_VFS_OK)
		(get_content_from_card (card)->dirty) = TRUE;

	UNLOCK_RIO();

	return res;
}

static GnomeVFSResult
do_check_same_fs (GnomeVFSMethod *method,
		  GnomeVFSURI *a,
		  GnomeVFSURI *b,
		  gboolean *same_fs_return,
		  GnomeVFSContext *context)
{
	DEBUG_RIO (("do_check_same_fs()\n"));
	
	*same_fs_return = FALSE;

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_move (GnomeVFSMethod *method,
	 GnomeVFSURI *old_uri,
	 GnomeVFSURI *new_uri,
	 gboolean force_replace,
	 GnomeVFSContext *context)
{
  	GnomeVFSResult res;
	int old_card, old_folder, old_song;
	int new_card, new_folder, new_song;
	gboolean move_folder = FALSE;
	char *new_name;

	g_warning("This function of the rio500 method is not properly supported\nPlease mail hadess@hadess.net with information about your program\n");

	DEBUG_RIO (("do_move() %s %s\n",
		    gnome_vfs_uri_to_string (old_uri, 0),
		    gnome_vfs_uri_to_string (new_uri, 0)));
	LOCK_RIO();

	res = lookup_uri(old_uri, &old_card, &old_folder, &old_song);
	DEBUG_RIO(("do_move(): lookup (src) %d %d %d\n",
				old_card, old_folder, old_song));
	/* if the original file isn't found, bail */
	if (res != GNOME_VFS_OK)
	{
		UNLOCK_RIO();
		return res;
	}

	res = lookup_uri(new_uri, &new_card, &new_folder, &new_song);
	DEBUG_RIO(("do_move(): lookup (dest) %d %d %d\n",
				new_card, new_folder, new_song));

	/* if the dest file is found and force_replace is false, bail */
	if (res == GNOME_VFS_OK && force_replace == FALSE)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_FILE_EXISTS;
	} else {
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_NOT_SUPPORTED;
	}

	/* if the dest uri is invalid, bail */
	if (res == GNOME_VFS_ERROR_INVALID_URI)
	{
		UNLOCK_RIO();
		return res;
	}

	/* if the move crosses disks, or tries to the rename the cards, bail */
	if (new_card != old_card || new_card == -1)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_NOT_SUPPORTED;
	}

	/* Moving songs */
	if (old_song != -1)
	{
		/* are they in the same folder ? */
		if (old_folder != new_folder)
		{
			UNLOCK_RIO();
			return GNOME_VFS_ERROR_NOT_SUPPORTED;
		}
		move_folder = FALSE;
	}

	rio->rio_hw = rio_new();
	if (rio_check(rio->rio_hw) == FALSE)
	{
		DEBUG_RIO(("do_move: check of rio is FALSE, destroying\n"));
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_IO;
	}
	rio_set_card(rio->rio_hw, old_card);

	/* Getting the name of the file/folder */
	{
		char *tmp;

		tmp = gnome_vfs_uri_get_basename(new_uri);
		if (tmp != NULL)
			new_name = tmp;
		else
			new_name = gnome_vfs_unescape_string(tmp, "/");
	}

	if (move_folder == TRUE)
	{
		res = rio_to_vfs_error(rio_rename_folder(rio->rio_hw,
					old_folder, (char *)new_name));
	} else {
		res = rio_to_vfs_error(rio_rename_song(rio->rio_hw,
					old_folder, old_song,
					(char *)new_name));
	}
	rio_delete(rio->rio_hw);

	if (res == GNOME_VFS_OK)
	{
		(get_content_from_card (old_card)->dirty) = TRUE;
		(get_content_from_card (new_card)->dirty) = TRUE;
	}

	UNLOCK_RIO();

	return res;
}


static GnomeVFSResult
do_truncate_handle (GnomeVFSMethod *method,
		    GnomeVFSMethodHandle *method_handle,
		    GnomeVFSFileSize where,
		    GnomeVFSContext *context)

{
	DEBUG_RIO(("do_truncate_handle\n"));
	return GNOME_VFS_ERROR_NOT_SUPPORTED;
}

static GnomeVFSResult
do_make_directory (GnomeVFSMethod *method,
		   GnomeVFSURI *uri,
		   guint perm,
		   GnomeVFSContext *context)
{
	GnomeVFSResult res;
	int card, folder, song;
	const char *folder_name = NULL;

	res = lookup_uri(uri, &card, &folder, &song);
	DEBUG_RIO(("do_make_directory(): lookup %d %d %d\n",
				card, folder, song));
	LOCK_RIO();
	if (res == GNOME_VFS_OK)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_FILE_EXISTS;
	}
	if (res == GNOME_VFS_ERROR_IO)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_IO;
	}

	if (res == GNOME_VFS_ERROR_INVALID_URI
			|| card == -1)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_NOT_PERMITTED;
	}

	if (folder != -1)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_NOT_PERMITTED;
	}

	rio->rio_hw = rio_new();
	if (rio_check(rio->rio_hw) == FALSE)
	{
		DEBUG_RIO(("do_make_directory: check of rio is FALSE, destroying\n"));
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_IO;
	}
	rio_set_card(rio->rio_hw, card);
	{
		const char *tmp;

		tmp = gnome_vfs_uri_get_basename(uri);
		if (tmp == NULL)
			folder_name = tmp;
		else
			folder_name = gnome_vfs_unescape_string(tmp, "/");
	}
	res = rio_to_vfs_error(rio_add_folder(rio->rio_hw,
			(char *)folder_name));
	rio_delete(rio->rio_hw);

	if (res == GNOME_VFS_OK)
		(get_content_from_card (card)->dirty) = TRUE;

	UNLOCK_RIO();

	return res;
}

static GnomeVFSResult
do_remove_directory (GnomeVFSMethod *method,
		     GnomeVFSURI *uri,
		     GnomeVFSContext *context)
{
	GnomeVFSResult res;
	int card, folder, song;
	GList *list;

	res = lookup_uri(uri, &card, &folder, &song);
	DEBUG_RIO(("do_remove_directory(): lookup %d %d %d\n",
				card, folder, song));

	LOCK_RIO();
	if (res != GNOME_VFS_OK)
	{
		UNLOCK_RIO();
		return res;
	}

	if (folder == -1)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_ACCESS_DENIED;
	}

	if (song != -1)
	{
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_NOT_A_DIRECTORY;
	}

	rio->rio_hw = rio_new();
	if (rio_check(rio->rio_hw) == FALSE)
	{
		DEBUG_RIO(("do_remove_directory: check of rio is FALSE\n"));
		UNLOCK_RIO();
		return GNOME_VFS_ERROR_IO;
	}
	rio_set_card(rio->rio_hw, card);
	list = rio_get_content_from_card (card);
	/* Format the card when deleting the last directory */
	if (g_list_length (list) == 1)
		res  = rio_to_vfs_error(rio_format(rio->rio_hw));
	else
		res = rio_to_vfs_error(rio_del_folder(rio->rio_hw, folder));
	rio_delete(rio->rio_hw);

	if (res == GNOME_VFS_OK)
		(get_content_from_card (card)->dirty) = TRUE;

	UNLOCK_RIO();

	return res;
}

static GnomeVFSResult
do_set_file_info (GnomeVFSMethod *method,
		  GnomeVFSURI *uri,
		  const GnomeVFSFileInfo *info,
		  GnomeVFSSetFileInfoMask mask,
		  GnomeVFSContext *context)
{
	GnomeVFSResult res = GNOME_VFS_OK;
	int card, folder, song;
	gboolean rename_folder = FALSE;
	const char *new_name;

	DEBUG_RIO (("do_set_file_info: mask %d\n", mask));

	if (mask &
		(GNOME_VFS_SET_FILE_INFO_PERMISSIONS |
		GNOME_VFS_SET_FILE_INFO_OWNER |
		GNOME_VFS_SET_FILE_INFO_TIME))
			return GNOME_VFS_ERROR_NOT_SUPPORTED;

	if (mask & GNOME_VFS_SET_FILE_INFO_NAME) {
		new_name = gnome_vfs_unescape_string(info->name, "/");
		DEBUG_RIO (("set_info: set new name: %s\n", new_name));

		res = lookup_uri(uri, &card, &folder, &song);
		DEBUG_RIO(("do_set_file_info(): lookup %d %d %d\n",
					card, folder, song));

		LOCK_RIO();
		if (res != GNOME_VFS_OK)
		{
			UNLOCK_RIO();
			return res;
		}

		if (folder == -1)
		{
			UNLOCK_RIO();
			return GNOME_VFS_ERROR_ACCESS_DENIED;
		}

		if (song == -1)
			rename_folder = TRUE;

		{
			GnomeVFSURI *new_uri;
			char *tmp, *new_filename;
			int new_card, new_folder, new_song;

			tmp = gnome_vfs_uri_extract_dirname(uri);
			DEBUG_RIO(("do_set_file_info(): dirname %s\n",
						tmp));
			new_filename = g_strdup_printf("%s%s",
					tmp, new_name);
			DEBUG_RIO(("do_set_file_info(): filename %s\n",
						new_filename));
			new_uri = gnome_vfs_uri_new(new_filename);
			
			UNLOCK_RIO();
			res = lookup_uri(new_uri, &new_card, &new_folder,
					&new_song);
			LOCK_RIO();

			g_free(new_filename);
			g_free(tmp);
			gnome_vfs_uri_unref(new_uri);
			
			if (res != GNOME_VFS_ERROR_NOT_FOUND)
			{
				UNLOCK_RIO();
				return GNOME_VFS_ERROR_FILE_EXISTS;
			}
		}

		rio->rio_hw = rio_new();
		if (rio_check(rio->rio_hw) == FALSE)
		{
			DEBUG_RIO(("do_set_file_info: check of rio is FALSE, destroying\n"));
			UNLOCK_RIO();
			return GNOME_VFS_ERROR_IO;
		}
		rio_set_card(rio->rio_hw, card);

		if (rename_folder == TRUE)
		{
			res = rio_to_vfs_error(rio_rename_folder(rio->rio_hw,
						folder, (char *)new_name));
		} else {
			res = rio_to_vfs_error(rio_rename_song(rio->rio_hw,
						folder, song,
						(char *)new_name));
		}
		rio_delete(rio->rio_hw);

		if (res == GNOME_VFS_OK)
			(get_content_from_card (card)->dirty) = TRUE;

		UNLOCK_RIO();
	}

	return res;
}

static GnomeVFSMethod method = {
	sizeof (GnomeVFSMethod),
	do_open,
	do_create,
	do_close,
	do_read,
	do_write,
	NULL, /* do_seek */
	NULL, /* do_tell */
	do_truncate_handle,
	do_open_directory,
	do_close_directory,
	do_read_directory,
	do_get_file_info,
	NULL, /* do_get_file_info_from_handle */
	do_is_local,
	do_make_directory,
	do_remove_directory,
	do_move,
	do_unlink,
	do_check_same_fs,
	do_set_file_info,
	NULL, /* do_truncate */
	NULL, /* do_find_directory */
	NULL  /* do_create_symbolic_link */
};

GnomeVFSMethod *
vfs_module_init (const char *method_name, const char *args)
{
	rio_lock = g_mutex_new();
#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif
	DEBUG_RIO (("<-- rio500 module init called -->\n"));

	/* Make sure we don't have a dangling temp file */
	unlink ("/tmp/rio500_tmp_file.mp3");

	/* Create the data structures */
	rio = g_new(RioTreeContent, 1);
	rio->rio_hw = NULL;
	rio->internal = rio->external = rio->virtual = NULL;

	return &method;
}

void
vfs_module_shutdown (GnomeVFSMethod *method)
{
	DEBUG_RIO (("<-- rio500 module shutdown called -->\n"));

	/* Destroy the temp file */
	unlink ("/tmp/rio500_tmp_file.mp3");
	destroy_rio(rio);
	g_mutex_free(rio_lock);
}

